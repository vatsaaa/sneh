import json
import logging

import requests
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Cursor
from tweepy import API

from config import twitter_cfg as twcfg

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


class TwitterStreamListener(StreamListener):
    def __init__(self):
        auth = OAuthHandler(twcfg.api_key, twcfg.api_secret_key)
        auth.set_access_token(twcfg.access_token, twcfg.access_token_secret)

        self.api = API(auth)
        try:
            self.api.verify_credentials()
        except Exception as e:
            logger.error("Error while initializing Twitter Stream Listener", exc_info=True)
            raise e
        finally:
            self.me = self.api.me()

    def on_status(self, status):
        # print(status.id)
        print(status.user.name, "|", status.text)
        return True

    def on_error(self, status_code):
        print('Got an error with status code: ' + str(status_code))
        return True  # To continue listening

    def on_timeout(self):
        print('Timeout...')
        return True  # To continue listening


def get_quote():
    params = {
        'method': 'getQuote',
        'lang': 'en',
        'format': 'json'
    }
    res = requests.get('http://api.forismatic.com/api/1.0/', params)
    json_text = json.loads(res.text)
    return json_text["quoteText"], json_text["quoteAuthor"]


# while True:
#     try:
#         quote,author = get_quote()
#         status = quote+" -"+author+"\n"+"#python \
#         #dailypython #twitterbot #pythonquotes #programming"
#         print('\nUpdating : ',status)
#         listener.api.update_status(status=status)
#         print("\nGoing to Sleep for 1 min")
#         time.sleep(60)
#     except Exception as ex:
#         print(ex)
#         break


def follow_followers(api):
    logger.info("Retrieving and following followers")
    for follower in Cursor(api.followers).items():
        if not follower.following:
            logger.info(f"Following {follower.name}")
            follower.follow()


def check_mentions(api, keywords, since_id):
    logger.info("Retrieving mentions")
    new_since_id = since_id
    for tweet in Cursor(api.mentions_timeline,
                           since_id=since_id).items():
        new_since_id = max(tweet.id, new_since_id)
        if tweet.in_reply_to_status_id is not None:
            continue
        if any(keyword in tweet.text.lower() for keyword in keywords):
            logger.info(f"Answering to {tweet.user.name}")

            if not tweet.user.following:
                tweet.user.follow()

            api.update_status(
                status="Please reach us via DM",
                in_reply_to_status_id=tweet.id,
            )
    return new_since_id

