import connexion
from flask_cors import CORS

ALLOW_ALL_FILE_TYPES = False

# Create the application instance and read swagger.yml to configure endpoints
app = connexion.App(__name__, specification_dir="./")
app.add_api("swagger.yml")

CORS(app.app, origins="*", allow_headers="*")

app.app.config["SECRET_KEY"] = 'top-secret!'
app.app.config["UPLOAD_FOLDER"] = "C:\\temp\\test"

app.app.config["LISTENER_HOST"] = "localhost"
app.app.config["LISTENER_PORT"] = "5667"

