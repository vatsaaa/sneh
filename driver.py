import sys
from typing import List

from tweepy import Stream

from twitter_handlers.TwitterStreamListener import TwitterStreamListener
from utils.driver_utils import process_args, usage
from cowin_handlers.CowinAccess import CowinAccess


def main(cwn, pin_codes, gml, ig, machine, port, twl, twtr, filters: List[str]):
    if cwn:
        try:
            ca = CowinAccess()
        except Exception as e:
            raise(e)
        finally:
            slots = ca.get_slots(state="Delhi", city="East Delhi")
            for s in slots:
                print("|"+s.get('name')+"|",
                      s.get('address')+"|",
                      s.get('district')+"|",
                      str(s.get('pin'))+"|",
                      s.get('capacity'))
    elif twtr:
        try:
            listener = TwitterStreamListener()
            stream = Stream(listener.api.auth, listener)
        except Exception as e:
            raise e
        finally:
            stream.filter(track=filters, languages=["en"])
    elif gml:
        print("Gmail...")
    elif ig:
        print("Instagram...")
    elif twl:
        print("Twilio...")
    else:
        print("Nothing to do...")


if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage()
        exit(-1)

    # TODO: Can we reduce the number of arguments here?
    cwn, pin_codes, gml, ig, machine, port, twl, twtr, filter_strings = process_args(sys.argv[1:])

    main(cwn, pin_codes, gml, ig, machine, port, twl, twtr, filter_strings)

