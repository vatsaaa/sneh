# Social NEtwork Handlers - SNEH

https://www.quandl.com/api/v3/datasets/WIKI/FB/data.xml?api_key=fQN4Wpx5Kc_RHFgKr9xy

https://levelup.gitconnected.com/how-to-create-a-python-api-using-flask-connexion-4f3fc77e7f6e


## References: 
    https://realpython.com/twitter-bot-python-tweepy/
    https://realpython.com/twitter-sentiment-python-docker-elasticsearch-kibana/

## Manual Testing:
    Swagger UI Url: http://localhost:5667/sneh-api/1.0.0/ui/
    
## Issues Resolution
### Issue 001:
`fatal: Authentication failed for https://...`
Reset the user creds with: `git config --system --unset credential.helper`

Then provide the correct password.