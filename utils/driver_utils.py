from sys import exit
import getopt


def usage():
    print("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-")
    print('Usage: python driver.py -c ...')
    print('Usage: python driver.py -g ...')
    print('Usage: python driver.py -h --help: Prints this help message')
    print('Usage: python driver.py -i ...')
    print('Usage: python driver.py -t [list of strings to filter tweets]')
    print('Usage: python driver.py -w ...')

    return


def usage_ws():
    print("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-")
    print("python3 driver_ws.py [-h|--help]: Print this help message")
    print("python3 driver_ws.py [-m|--machine]: Machine on which this service runs")
    print("python3 driver_ws.py [-p|--port]: Port on which this service runs")


def process_args(args):
    cwn = False
    twtr = False
    gml = False
    ig = False
    twl = False

    filter_strings = ""
    pin_codes = []

    machine = None
    port = None

    try:
        opts, args = getopt.getopt(args, "c:g:hi:m:p:t:w:",
                                   [
                                       "cowin=",
                                       "gmail=",
                                       "help",
                                       "instagram=",
                                       "machine="
                                       "port="
                                       "twitter=",
                                       "twilio="
                                   ])
    except getopt.GetoptError as err:
        print("Error: " + str(err))
        usage()
        exit(-2)

    ''' 
    TODO: Only one handlers can be specified 
    raise an exception if more than one is True
    '''
    for o, a in opts:
        if o in ("-c", "--cowin"):
            cwn = True
            pin_codes = a
        elif o in ("-g", "--gmail"):
            gml = True
            filter_strings = a
        elif o in ("-h", "--help"):
            usage()
            exit()
        elif o in ("-i", "--instagram"):
            ig = True
            filter_strings = a
        elif o in ("-m", "--machine"):
            machine = a
        elif o in ("-p", "--port"):
            port = a
        elif o in ("-t", "--twitter"):
            twtr = True
            filter_strings = a
        elif o in ("-w", "--twilio"):
            twl = True
            filter_strings = a
        else:
            assert False, "Invalid option: " + o + "provided!"

    return cwn, pin_codes, gml, ig, machine, port, twl, twtr, filter_strings


