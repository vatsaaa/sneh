from cowin_api import CoWinAPI
from datetime import date


class CowinAccess:
    def __init__(self):
        self.cowin = CoWinAPI()

    def get_state_id(self, state: str):
        states = self.cowin.get_states()

        state_id = None

        for s in states.get('states'):
            if state.lower() == (s.get('state_name')).lower():
                state_id = s.get('state_id')
                break

        return state_id

    def get_city_id(self, state_id: int, city: str):
        cities = self.cowin.get_districts(state_id)

        city_id = ""

        for c in cities.get('districts'):
            if city.lower() == (c.get('district_name')).lower():
                city_id = c.get('district_id')
                break

        return city_id

    def get_slots(self, state: str, city: str):
        state_id = self.get_state_id(state)
        city_id = self.get_city_id(state_id, city)

        centres = self.cowin.get_availability_by_district(city_id.__str__(),
                                                          date.today().strftime('%d-%m-%Y'),
                                                          18)

        slots = []

        for c in centres.get('centers'):
            if c.get('fee_type') == 'Free':
                    for cs in c.get('sessions'):
                        if cs.get('vaccine') == 'COVISHIELD' \
                                and cs.get('available_capacity_dose1') > 0:
                            slots.append({'name': c.get('name'),
                                          'address': c.get('address'),
                                          'district': c.get('district_name'),
                                          'pin': c.get('pincode'),
                                          'capacity': cs.get('available_capacity_dose1')
                                          })

        return slots


# Haryana 12, Tamil Nadu 31, Delhi 9


haryana = 12
tamilnadu = 31
delhi = 9

gurgaon = '188'
chennai = '571'

# gurgaon_centers = cowin.get_availability_by_district(gurgaon, date.today().strftime('%d-%m-%Y'), 18)
# chennai_centers = cowin.get_availability_by_district(chennai, date.today().strftime('%d-%m-%Y'), 18)
