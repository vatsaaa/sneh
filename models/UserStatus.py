import enum


class UserStatus(enum.Enum):
    UserActive = 1
    UserInActive = 2

