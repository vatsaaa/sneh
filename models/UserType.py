import enum


class UserType(enum.Enum):
    Donor = 1
    DonorRelated = 2
    Receiver = 3
    ReceiverRelated = 4

