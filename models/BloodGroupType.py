import enum


class BloodGroupType(enum.Enum):
    APositive = 1
    ANegative = 2

    BPositive = 3
    BNegative = 4

    OPositive = 5
    ONegative = 6

    ABPositive = 7
    ABNegative = 8
