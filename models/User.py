import datetime

from models import UserType
from models.BloodGroupType import BloodGroupType


class User:
    def __init__(self, db, whatday: datetime.date, utyp: UserType,
                 location: str, bgrp: BloodGroupType, eml: str=None,
                 pnum: int=None, prscrptn: str=None):
        self.db = db
        self.cdate = whatday if whatday else datetime.date.today()  # TODO: Single & correct date format
        self.utyp = utyp if utyp else None
        self.phone = pnum if pnum else None
        self.email = eml if eml else None
        self.location = location if location else None
        self.prescription = prscrptn if prscrptn else None
        self.bgrp = bgrp if bgrp else None

    # DB Functions
    def save(self):
        users_collection = self.db["users"]

        user = {
            "created": self.cdate,
            "user_type": self.utyp,
            "phone": self.phone,
            "email": self.email,
            "location": "Delhi",
            "prescription": "",
            "blood_group": self.bgrp
        }

        x = None

        try:
            x = users_collection.insert_one(user)
        except Exception as e:
            print(e)
        finally:
            return x.inserted_id

    def find(self, sdate, edate):
        users_collection = self.db["users"]

        matches = []

        for u in users_collection.find({}):
            print(u)
            # matches.append(u)

        return matches
