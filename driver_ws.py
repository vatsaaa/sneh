import sys

from config.sneh_config import app
from utils.driver_utils import process_args, usage_ws

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage_ws()
        exit(1)

    gml, ig, machine, port, twl, twtr, filter_strings = process_args(sys.argv[1:])

    app.run(host=machine if machine is not None else app.app.config["LISTENER_HOST"],
            port=port if port is not None else app.app.config["LISTENER_PORT"],
            debug=True)
