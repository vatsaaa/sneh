from models.User import User
from models.UserType import UserType
from models.BloodGroupType import BloodGroupType

from datetime import date

from pymongo import MongoClient
from flask import request

# TODO: Implement a Singleton here? Or is there a different strategy
# TODO: Move this to config then? Or a setup-file?
try:
    client = MongoClient(
        "mongodb+srv://dbadmin:dbadmin@cluster0.vp8vq.mongodb.net/snehdb?retryWrites=true&w=majority")
except Exception as e:
    print(e)
    exit(-3)
finally:
    db = client.snehdb


def search(ut: UserType, loc: str, bg: BloodGroupType, sd: date=None, ed: date=None):
    u2s = User(db, whatday=date.today(), utyp=ut, location=loc, bgrp=bg)

    return u2s.find(sd, ed)


def add():
    req_json = request.get_json()
    try:
        u = User(db, whatday=req_json.get("whatday"), utyp=req_json.get("usrtyp"),
                 pnum=request.json.get("phonenum"), eml=req_json.get("eml"),
                 location=req_json.get("loc"), prscrptn=req_json.get("prscrptn"),
                 bgrp=req_json.get("bgtyp"))
    except Exception as e:
        print(e)
    finally:
        u.save()
